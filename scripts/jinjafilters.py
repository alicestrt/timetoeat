from itertools import zip_longest
from urllib.parse import quote as urlquote
import time
from markdown import markdown as markdown_
import json as json_

def datetimeformat (t, format='%Y-%m-%d %H:%M:%S'):
    return time.strftime(format, time.localtime(t))

def append (t, at, rstrip=True):
    if rstrip:
        return t.rstrip() + at
    else:
        return t+at

def append_edit_link (t, url=None):
    # <a href="/cgi-bin/aa/indexedit.cgi?url={{c.url|urlencode}}">
    if url:
        return t.rstrip() + '<a href="/cgi-bin/aa/indexedit.cgi?url={0}">&#9997;</a>'.format(urlquote(url))
    else:
        return t.rstrip() + '<a href="/cgi-bin/aa/indexedit.cgi">&#9997;</a>'

def grouper(iterable, n, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)

def humanize_bytes(bytesize, precision=2):
    """
    Humanize byte size figures
    """
    abbrevs = (
        (1 << 50, 'PB'),
        (1 << 40, 'TB'),
        (1 << 30, 'GB'),
        (1 << 20, 'MB'),
        (1 << 10, 'kB'),
        (1, 'bytes')
    )
    if bytesize == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if bytesize >= factor:
            break
    if factor == 1:
        precision = 0
    return '%.*f %s' % (precision, bytesize / float(factor), suffix)

import math

def timecode(rawsecs, fract=True, alwaysfract=True, fractdelim='.', alwayshours=True):
    # returns a string in HH:MM:SS[.xxx] notation
    # if fract is True, uses .xxx if either necessary (non-zero)
    # OR alwaysfract is True
    hours = math.floor(rawsecs / 3600)
    rawsecs -= hours * 3600
    mins = math.floor(rawsecs / 60)
    rawsecs -= mins * 60
    if fract:
        secs = math.floor(rawsecs)
        rawsecs -= secs
        if (rawsecs > 0 or alwaysfract):
            fract = "%.03f" % rawsecs
            if hours or alwayshours:
                return "%02d:%02d:%02d%s%s" % (hours, mins, secs, fractdelim, \
                        fract[2:])
            else:
                return "%02d:%02d%s%s" % (mins, secs, fractdelim, fract[2:])
        else:
            if hours or alwayshours:
                return "%02d:%02d:%02d" % (hours, mins, secs)
            else:
                return "%02d:%02d" % (mins, secs)

    else:
        secs = round(rawsecs)
        if hours or alwayshours:
            return "%02d:%02d:%02d" % (hours, mins, secs)
        else:
            return "%02d:%02d" % (mins, secs)

def markdown (src):
    return markdown_(src)

def json (src):
    return json_.dumps(src, indent=2)

all = {
    'datetimeformat': datetimeformat,
    'grouper': grouper,
    'timecode': timecode,
    'humanize_bytes': humanize_bytes,
    'urlquote': urlquote,
    'markdown': markdown,
    'append': append,
    'json': json,
    'append_edit_link': append_edit_link
}