"""

Implementation of the Temporal Dimension portion of Media Fragments 1.0
https://www.w3.org/TR/media-frags/

"""
from __future__ import print_function
import re, math, collections
try:
    # py2
    from urlparse import urlparse, urlunparse
except ImportError:
    # py3
    from urllib.parse import urlparse, urlunparse

TEMPORAL_FRAGMENT_PATTERN = re.compile(r"""
        t=              # the pattern always starts with t=
        (?:npt:)?       # optional prefix, npt, or normal play time, is the default scheme
        (?: (?:(\d+)[:])?(\d\d)[:](\d\d)(?:\.(\d+))? |      # raw number of seconds, optional fraction
            (\d+(?:\.(?:\d+))?) )?                          # OR timecode format, h:mm:ss.fract       
        (?: ,                                               # optional comma separating start & end
            (?: (?:(\d+)[:])?(\d\d)[:](\d\d)(?:\.(\d+))? |  # raw number of seconds, optional fraction
                (\d+(?:\.(?:\d+))?) ))?                     # OR timecode format, h:mm:ss.fract       
    """, re.X)

TemporalInterval = collections.namedtuple('TemporalInterval', ['start', 'end'])

def parse_temporal_fragment (fragment):
    """
    parses temporal specifiers and returs a tuple.
    Returns (start, end) or None (no match)
        Start will never be None (since it's 0 when omitted)
        End may be None (when it's not specified) which should be interpreted as meaning "until the end"

    examples:

        t=10.12345 => (10.12345, None)
        t=10,20 => (10.0, 20.0)
        t=,20 => (0.0, 20.0)
        t=10 => (10.0, None)
        t=npt:10,20 => (10.0, 20.0)
        t=npt:,121.5 => (0.0, 121.5)
        t=0:02:00,121.5 => (120.0, 121.5)
        t=npt:120,0:02:01.5 => (120.0, 121.5)
        t=120:01:02 => (432062.0, None)
        t=foccacia => None
        potato => None

    """
    def int_or_zero (x):
        if x == None:
            return 0
        else:
            return int(x)

    def to_seconds(h, m, s, fract):
        return (h*3600)+(m*60)+s+(float("0."+str(fract)))

    def parse_npt_tuple (x):
        if x[0] == None and x[1] == None and x[2] == None and x[3] == None and x[4] == None:
            return None
        if (x[4] != None):
            # raw seconds
            return float(x[4])
        else:
            # timecode
            return to_seconds(*[int_or_zero(n) for n in x[:4]])

    m = TEMPORAL_FRAGMENT_PATTERN.search(fragment)
    if m:
        g = m.groups()
        start, end = parse_npt_tuple(g[:5]), parse_npt_tuple(g[5:])
        if start == None and end == None:
            return None
        if start == None:
            start = 0.0
        return TemporalInterval(start, end)

def parseurl (url, usedict=None):
    """ base is the scheme+netloc+path of the primary resources, e.g. http://archive.org/path/video.webm """
    if usedict == None:
        usedict = {}
    ret = usedict
    ret['url'] = url
    parts = urlparse(url)
    ret['scheme'], ret['netloc'], ret['path'], ret['params'], ret['query'], ret['fragment'] = parts
    ret['base'] = urlunparse((ret['scheme'], ret['netloc'], ret['path'], None, None, None))
    if ret['fragment']:
        t = parse_temporal_fragment(ret['fragment'])
        if t:
            ret['t'] = t
    return ret

def npt_fromsecs(rawsecs):
    """ produces 00:10 00:10.500 """
    if rawsecs < 60:
        return str(rawsecs)
    hours = math.floor(rawsecs / 3600)
    rawsecs -= hours * 3600
    mins = math.floor(rawsecs / 60)
    rawsecs -= mins * 60
    secs = math.floor(rawsecs)
    rawsecs -= secs
    if (rawsecs > 0):
        fract = "%.03f" % rawsecs
        # fract = str(rawsecs)
        if hours:
            return "%02d:%02d:%02d.%s" % (hours, mins, secs, fract[2:])
        else:
            return "%02d:%02d.%s" % (mins, secs, fract[2:])
    else:
        if hours:
            return "%02d:%02d:%02d" % (hours, mins, secs)
        else:
            return "%02d:%02d" % (mins, secs)

class MediaFragment (dict):
    """ Convenience class that provides a nifty split method """

    def __init__ (self, url, t=None, defaults=True):
        parseurl(url, self)
        # optional t overrides
        if t != None:
            self['t'] = t
        if defaults:
            if 't' not in self:
                self['t'] = TemporalInterval(0, None)

    def __repr__ (self):
        frag = None
        if 't' in self:
            frag = "t={0}".format(npt_fromsecs(self['t'].start))
            if self['t'].end:
                frag += ",{0}".format(npt_fromsecs(self['t'].end))
        return urlunparse((self['scheme'], self['netloc'], self['path'], None, None, frag))

    def __hash__ (self):
        return hash(self.__repr__())

    def __add__(self, delta):
        return MediaFragment(self['url'], t=TemporalInterval(self['t'].start+delta.tvalue, None))

    def __sub__ (self, fragment_or_delta):
        if type(fragment_or_delta) == MediaFragment:
            return MediaFragmentDelta(self['t'].start-fragment_or_delta['t'].start)
        else:
            return MediaFragment(self['url'], t=TemporalInterval(self['t'].start-fragment_or_delta.tvalue, None))

    def __mul__ (self, fragment):
        """ intersection? """
        ts, te = self['t']
        os, oe = fragment['t']
        ns = max(ts, os)
        if te == None:
            if oe == None:
                ne = None
            else:
                ne = oe
        else:
            if oe == None:
                ne = te
            else:
                ne = min(te, oe)
        if ne == None or ns < ne:
            return MediaFragment(self['url'], t = TemporalInterval(ns, ne))

    def split(self, dur):
        """
        dur is interpreted as a durational value
        cut the HREF in 2, returns a pair of hrefs one of which can be None if the original fragment falls completely on one side of the cut
        """
        base = self['base']
        if 't' in self:
            t = self['t'].start + dur
            if (self['t'].end != None and t < self['t'].end):
                return (MediaFragment(base, t=TemporalInterval(self['t'].start, t)), MediaFragment(base, t=TemporalInterval(t, self['t'].end)))
            else:
                return (self, None)
        else:
            t=dur
            return (MediaFragment(base, t=TemporalInterval(0, t)), MediaFragment(base, t=TemporalInterval(t,None)))

class MediaFragmentDelta (object):
    def __init__(self, d):
        self.tvalue = d

if __name__ == "__main__":
    print ("Fragment tests")
    ftests= """
t=10.12345
t=10,20
t=,20
t=10
t=npt:10,20
t=npt:,121.5
t=0:02:00,121.5
t=npt:120,0:02:01.5
t=120:01:02
t=foccacia
potato
"""

    # for t in ftests.strip().splitlines():
    #     print (t, "=>", parse_temporal_fragment(t))
    # print ()

    tests = """
http://video.constantvzw.org/Cyberfeminisme/INTERACT.webm#t=00:22.55533,00:26
foo
foo#t=10,20
foo#t=,20
foo#t=10
foo#t=npt:10,20
foo#t=npt:,121.5
foo#t=0:02:00,121.5
foo#t=npt:120,0:02:01.5

"""

    # from pprint import pprint
    # print ("URL/MediaFragment tests")
    # for t in tests.strip().splitlines():
    #     print ("MediaFragment")
    #     frag = MediaFragment(t.strip())
    #     print (frag)
    #     print ("SPLIT")
    #     a, b = frag.split(1.5)
    #     print (" ", a)
    #     print (" ", b)
    #     print ()

    MF = MediaFragment
    # a = MF("A#t=10")
    # b = MF("B")
    # c = MediaFragment("C#t=30")

    # print (c + (b - a)) 

    x = MF("#t=30,100")
    y = MF("#t=20,120")
    print ((x*y).split(10))