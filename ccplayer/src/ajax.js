function get (url, callback) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        callback(null, request.responseText);
      } else {
        callback("server error", null);
      }
    };
    request.onerror = function() {
      callback("connection error", null);
    };
    request.send();
}

module.exports.get = get;