* Try using SVG to make "editable" layout?
* Generate an SVG with image (tiles) to test a grid.
* Alternatively: define the layout in text format... csv ?! BUT ... visually it's easier to layout.


Barcelona
==================
phase 1
* all six issues
* primarily video mode with scans as thumbs to trigger someone's physical manipulation of the magazine
* local server with all six

later
* second layer
* wiki + desktop + 

* vitrine to "play" the material via faces.... (invert the vitrine to attach faces (and movement) to faces in photos ?!)
* vitrine on raspi's ... or laptops ?
* use on scans


Thoughts Leaving Barcelona 19 feb 2018
=====================

* Installation of local server / VPN 
* Makeserver based editing ?! (evt adding wiki as "backend")


Skype April 2018
=======================
Make prototype public!!! (no password for testing of tablets)
Buy pi + cam + router and bill ... (less than 100 EUR)
Test pi/vitrine...
fallback ... use laptop + vitrine
keep the same paradigm...
<1> pinball photos ... look for faces in those...
3rd faces ... on a third monitor?
? how many monitors ? (1 or 2, ellef will find them)

May 2018
============

Matching