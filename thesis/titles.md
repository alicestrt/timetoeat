[vid#t=00:00:52.746]
[scans#6/-4.234/90.898]
Fire has led to the development of cooking, and thus our civilization

[scans#7/-4.789/195.059]
[vid#t=00:02:42.991]
Martha Rosler's piece came as a criticism towards the image of the jolly homecook, performed by Julia Child in her TV show 'The French Chef'. Put side by side, they seem to be part of the same performance.

[scans#6/-3.625/243.602]
[vid#t=00:03:55.416]
Food that seems to appear out of thin air is a common element of popular culture which depicts the future - either utopian, or dystopian.




















[scans]: http://localhost:8080/thesis/scans.html "scans"
[vid]: http://localhost:8080/thesis/test_cut.webm "video"