var timecode = require("./timecode.js"),
    EventEmitter = require("eventemitter3");

function parse_fragment (url) {
    // console.log("parse_fragment", url);
    ret = {}
    if (url.indexOf("#") >= 0) {
        var p = url.split("#", 2);
        ret.base = p[0];
        ret.fragment = p[1];
    } else {
        ret.base = url;
        ret.fragment = '';
    }
    return ret;
}
function parseTime (h) {
    var m = h.match("t=([^&,]*)");
    // console.log("parseTime", h, m);
    if (m) { return timecode.timecode_to_seconds(m[1]); }
}

class CCFrameSimple {
    constructor (elt) {
        // console.log("ccframe.init", elt);
        this.elt = elt;
        // console.log("CCFrameSimple", this.elt, this.elt.nodeName);
        if (this.elt.nodeName == "VIDEO" || this.elt.nodeName == "IFRAME") {
            var src = this.elt.getAttribute("src");
            // console.log("ccframe inited with src", src);
            if (src) {
                var purl = parse_fragment(src);
                this.url = src;
            }
            this.elt.addEventListener("load", this._load.bind(this));
            if (this.elt.nodeName == "VIDEO") {
                this.elt.addEventListener("timeupdate", this._timeupdate.bind(this)); 
            }
            if (this.elt.contentWindow) {
                this.elt.contentWindow.addEventListener("hashchange", function () {
                    // console.log("iframe hashchange", that.iframe.contentWindow.location.hash);
                    // $(that.elt).trigger("hashchange", that.iframe.contentWindow.location.hash);
                    that.events.emit("hashchange", that.elt.contentWindow.location.hash);
                    // new: may 2018, update my own source to reflect changed hash!
                    that._update_hash(that.elt.contentWindow.location.hash);
                })
            }

        } else {
            console.log("WARNING, unsupported element type", this.elt.nodeName);
        }
        var that = this;
        this.events = new EventEmitter();
        // console.log("ccframesimple.events", this.events);
    }
    on (event, callback) {
        this.events.on(event, callback);
        return this;
    }
    set src (url) {
        // console.log("ccframe.src", url);
        var purl = parse_fragment(url),
            previous_url = this.url ? parse_fragment(this.url) : null;

        this.url = url;
        if (!previous_url || previous_url.base != purl.base) {
            this.events.emit("srcchange", purl.base);
        }
        if (this.elt.nodeName == "IFRAME") {
            if (previous_url && previous_url.base == purl.base) {
                // fragment change only
                // console.log("ccframesimple.iframe fragment update", purl.fragment)
                this.elt.contentWindow.location.hash = purl.fragment;
            } else {
                console.log("ccframesimple setting src", url);
                this.elt.src = url;
            }
        } else {
            if (previous_url && previous_url.base == purl.base) {
                var t = parseTime(purl.fragment);
                if (t !== undefined) {
                    // console.log("ccframesimple.media currentTime update", t)
                    this.elt.currentTime = t;
                    this.elt.play();
                } else {
                    console.log("ccframe, bad timecode?", purl.fragment);
                }
            } else {
                console.log("ccframesimple setting src", url);
                this.elt.src = url;
                this.elt.play();
            }
        }
        return this;
    }
    get src () {
        return this.url;
    }
    get base () {
        return parse_fragment(this.url).base;
    }
    _load (e) {
        // console.log("ccframe._load", this, e, this.url);
        if (this.elt.contentDocument) {
            var video = this.elt.contentDocument.querySelector("video");
            if (video) {
                // console.log("iframe has video", video);
            }
            // var pageNumber = this.elt.contentDocument.querySelector("input#pageNumber");
            // this.pageNumberInput = pageNumber;
            // this.pageNumber = pageNumber && pageNumber.value;
            // if (pageNumber) {
            //     // console.log("iframe has pageNumber", pageNumber);
            //     this.interval = function () {
            //         if (this.pageNumberInput.value != this.pageNumber) {
            //             this.pageNumber = this.pageNumberInput.value;
            //             // console.log("page changed", this.pageNumber);
            //         }
            //     };
            //     window.setInterval(this.interval.bind(this), 1000);
            // }
        } else {
            // console.log("unable to access iframe.contentDocument");
        }

        if (this.elt.contentWindow) {
            var that = this;
            // console.log("watching iframe.contentWindow")
            this.elt.contentWindow.addEventListener("hashchange", function () {
                // console.log("iframe hashchange", that.iframe.contentWindow.location.hash);
                // $(that.elt).trigger("hashchange", that.iframe.contentWindow.location.hash);
                that.events.emit("hashchange", that.elt.contentWindow.location.hash);
                // new: may 2018, update my own source to reflect changed hash!
                that._update_hash(that.elt.contentWindow.location.hash);
            })
        }
    }
    _update_hash (f) {
        this.url = parse_fragment(this.url).base+f;
        // console.log("updated hash", this.url);
    }
    _timeupdate (e) {
        var h = "#t="+timecode.seconds_to_timecode(this.elt.currentTime);
        this.events.emit("hashchange", h, this.elt.currentTime);
        // new: may 2018, update my own source to reflect changed hash!
        this._update_hash(h);
    }
    /* Functions for Ctrl Keys */
    toggle () {
        if (this.elt.play) {
            this.video.paused ? this.video.play() : this.video.pause();
        }
    }
    jumpforward () {
        if (this.elt.play) {
            this.video.currentTime += 5;
        }
    }
    jumpback () {
        if (this.elt.play) {
            this.video.currentTime -= 5;
        }
    }

    get currentTime () {
        if (this.elt.play) {
            return this.elt.currentTime;
        }
    }
    set currentTime (t) {
        if (this.elt.play) {
            this.elt.currentTime = t;
        }
    }
    
    get fragment () {
        // if (this.pageNumber !== null) {
        //     return "#page="+this.pageNumber;
        if (this.elt.currentTime) {
            return "#t="+timecode.seconds_to_timecode(this.elt.currentTime);
        } else {
            return this.elt.contentWindow.location.hash;
        }
    }
    set fragment (val) {
        
    }
    
}

module.exports = CCFrameSimple;
