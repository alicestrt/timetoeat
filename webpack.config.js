const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
    entry: "./src/editor.js",
    output: {
        filename: "cceditor.js",
        path: path.resolve(__dirname, 'dist'),
        library: "cceditor"
    }
    // plugins: [
    //     new UglifyJSPlugin()
    // ]
}