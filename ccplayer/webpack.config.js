const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
    entry: "./src/ccplayer.js",
    mode: "development",
    output: {
        filename: "ccplayer.js",
        path: path.resolve(__dirname, 'dist'),
        library: "ccplayer"
    }
    // plugins: [
    //     new UglifyJSPlugin()
    // ]
}