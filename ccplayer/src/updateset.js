function updateset () {
    var that = {},
        active_ids = {},
        actives = [];

    function copy (obj) {
        var ret = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) ret[attr] = obj[attr];
        }
        return ret;
    }

    function update (newitems, callbacks) {
        // console.log("update", newitems.map(function (x) { return x.id }));
        var exit_ids = copy(active_ids),
            enter = [],
            hold = [],
            change = false;

        for (var i=0, l=newitems.length; i<l; i++) {
            var ni = newitems[i];
            if (active_ids[ni.id] != undefined) {
                // hold
                if (callbacks && callbacks.hold) {
                    callbacks.hold.call(that, ni);
                }
                delete exit_ids[ni.id];
            } else {
                // enter
                change = true;
                active_ids[ni.id] = ni;
                if (callbacks && callbacks.enter) {
                    callbacks.enter.call(that, ni);
                }
            }
        }
        for (var id in exit_ids) {
            // exit
            change = true;
            if (exit_ids.hasOwnProperty(id)) {
                delete active_ids[id];
                if (callbacks && callbacks.exit) {
                    callbacks.exit.call(that, exit_ids[id]);
                }
            }
        }

        if (change) {
            actives = [];       
            for (var id in active_ids) {
                if (active_ids.hasOwnProperty(id)) {
                    actives.push(active_ids[id]);
                }
            }
            if (callbacks && callbacks.change) {
                callbacks.change.call(that, actives);
            }           
        }
    }
    that.update = update;
    return that;
}

module.exports = updateset;
