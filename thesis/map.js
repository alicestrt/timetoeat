var cell_width = 256,
    cell_height = 256;

var map = L.map('map', {
        maxZoom: 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = [
  {
    "id": "t1",
    "zoom": 6,
    "y": -4.234,
    "x": 90.898
  },
  {
    "id": "t2",
    "zoom": 7,
    "y": -4.789,
    "x": 195.059
  },
  {
    "id": "t3",
    "zoom": 6,
    "y": -3.625,
    "x": 243.602
  }
];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    console.log("data", data);
    var x = 0,
        y = 0,
        items = data['@graph'],
        pelts = [];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i];
        pelts.push({x: x, y: y, item: items[i]});
        console.log(item);
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

     map.addLayer(markers_layer);
     L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("could not load tiles.json");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();
