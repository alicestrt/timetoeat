  function _log () {
    return;
    var msg = "";
    for (var i=0, l=arguments.length; i<l; i++) {
      msg += arguments[i];
    }
    var mdiv = document.createElement("div");
    mdiv.textContent = msg;
    document.getElementById("debug").appendChild(mdiv, 0);
  }
  _log("page loading...");
  var frame = document.getElementById("fs");
  function fschange (e) {
    _log("fullscreenchange", document.mozFullScreen, document.webkitIsFullScreen, document.fullscreen);
    if (document.fullscreen === false || document.mozFullScreen === false || document.webkitIsFullScreen === false) {
      // console.log("leaving fullscreen");
      // frame.style.display = "none";
      frame.style.visibility = "hidden";
    }
  }
  function toggleFullScreen() {
    if (!document.fullscreen && !document.mozFullScreen && !document.webkitIsFullScreen) {
      // frame.style.display = "block";
      frame.style.visibility = "visible";
      if (frame.mozRequestFullScreen) {
        frame.mozRequestFullScreen();
      } else {
        frame.webkitRequestFullScreen();
      }
    } else {
      // frame.style.display = "none";
      frame.style.visibility = "hidden";
      if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else {
        document.webkitCancelFullScreen();
      }
    }
  }
  
  document.addEventListener("mozfullscreenchange", fschange, false);
  document.addEventListener("webkitfullscreenchange", fschange, false);
  document.getElementById("link").addEventListener("click", function (e) {
    toggleFullScreen();
    e.preventDefault();
    e.stopPropagation();
  }, false);
