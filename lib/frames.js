// console.log("ccplayer", ccplayer);
(function () {
    var ccp = ccplayer.ccplayer ({
        titles: "#titles",
        frames: {
            "video": "#video",
            "scans": "#scans"
        }
    });
    var titlesdiv = document.getElementById("titlesdiv"),
        textcontroldiv = document.getElementById("textcontroldiv");
    textcontroldiv.addEventListener("click", function () {
        titlesdiv.classList.toggle("hidden");
    });
})();
